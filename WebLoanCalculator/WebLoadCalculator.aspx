﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebLoadCalculator.aspx.cs" Inherits="WebCalculator.WebLoadCalculator" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style='clear:both'>
        <p>Interest Rate</p>
         <asp:TextBox ID="interestRateTextBox" runat="server" Style="text-align: right" Width="200px"></asp:TextBox>
        <p>Loan Amount</p>
        <asp:TextBox ID="loanAmountTextBox" runat="server" Style="text-align: right" Width="200px"></asp:TextBox>
         <p>Number of Monthly Payments</p>
         <asp:TextBox ID="numMonthlyPaymentTextBox" runat="server" Style="text-align: right" Width="200px"></asp:TextBox>
         <div style='clear:both'>
        <asp:Button ID="calculateButton" runat="server" Text="Calculate" Width="100" 
            onclick="calculateButton_Click" />
            </div>
    </div>
    <div style='clear:both'>
        <p>Result</p>
        <asp:TextBox ID="resultTextBox" runat="server" Style="text-align: right" Width="200px"></asp:TextBox>
    </div>
    </form>
</body>
</html>
