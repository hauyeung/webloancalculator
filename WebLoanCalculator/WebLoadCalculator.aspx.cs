﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebCalculator
{
    public partial class WebLoadCalculator : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private double calculatePayment(double interestRate, double loanAmount, int numberOfMonthlyPayments) 
        { 
            // Make sure we have valid data 
            if (interestRate > 0 && loanAmount > 0 && numberOfMonthlyPayments > 0) 
            { 
                // Check interest rate format, and divide into monthly percentage rate accordingly 
                if (interestRate > 0) 
                    interestRate /= 1200;
                else 
                    interestRate /= 12; 
                // Calculate and return loan payment amount 
                return (interestRate + (interestRate / (Math.Pow(1 + interestRate, numberOfMonthlyPayments) - 1))) * loanAmount; 
            } 
                else 
                { 
                    return 0; 
                } 
        }

        protected void calculateButton_Click(object sender, EventArgs e)
        {
            double irate = Convert.ToDouble(interestRateTextBox.Text);
            double loanamt = Convert.ToDouble(loanAmountTextBox.Text);
            int  nummthlypayments = Convert.ToInt32(numMonthlyPaymentTextBox.Text);
            resultTextBox.Text = Convert.ToString(calculatePayment(irate, loanamt, nummthlypayments));
        } 
    }
}